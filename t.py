import json
import requests

contacts = "https://levelsolar.secure.force.com/api/services/apexrest/contacts"
accounts = "https://levelsolar.secure.force.com/api/services/apexrest/accounts"
cases = "https://levelsolar.secure.force.com/api/services/apexrest/cases"

towns = {"Town of Hempstead": [{"item": "1", "name": "Triplicate Building Application", "count": "1"},
							  {"item": "2", "name": "Fast Track Application", "count": "1"},
							  {"item": "3", "name": "Workmans Compensation Insurance", "count": "1"},
							  {"item": "4", "name": "Certificate of Compliance", "count": "1"},
							  {"item": "5", "name": "Land Survey", "count": "3"},
							  {"item": "6", "name": "Construction Plans", "count": "2"},
							  {"item": "7", "name": "Manufacturer Data Sheets", "count": "2"}],
		 "Town of Babylon": [{"item": "1", "name": "Building Permit Application", "count": "1"},
							  {"item": "2", "name": "Property Owners Endoresement", "count": "1"},
							  {"item": "3", "name": "Solar Fast Track Checklist", "count": "1"},
							  {"item": "4", "name": "Solar Fast Track Permit Application", "count": "1"},
							  {"item": "5", "name": "Statement of Estimated Cost", "count": "1"},
							  {"item": "6", "name": "License & Insurance Pack", "count": "1"},
							  {"item": "7", "name": "Land Survey", "count": "3"},
							  {"item": "8", "name": "Construction Pack", "count": "2"}]}

if "Town of Babylont" in towns:
	jim = 80
else:
	jim = 60

print jim